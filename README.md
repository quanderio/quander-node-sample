# QUANDER NODE Integration example

## Get started

```sh
git clone git@bitbucket.org:quanderio/quander-node-sample.git
cd quander-node-sample
npm install
```

Run quander example:

```sh
export QUANDER_DOMAIN=testing
export QUANDER_APP_ID=3_195mrfc5wfms0wc884ocwkskkk8ogkgsk8kogw8ckcwwwk804s
export QUANDER_APP_SECRET=4225r5v6y1wk0kks4so0sso4g8ogss44k0kwg4k8co040oks84

DEBUG=quander-node-sample:* npm start
```

## About the application

This application has one route: [http://localhost:3000](http://localhost:3000)

Once log in (bottom right of the page) you have to select your account followed by your project, then you will be redirect to a your media for that project.