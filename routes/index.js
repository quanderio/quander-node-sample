var express = require('express');
var Quander = require('quander-node-sdk').Quander;
var QuanderApiError = require('quander-node-sdk').QuanderApiError;
var QuanderSdkError = require('quander-node-sdk').QuanderSdkError;
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.twig', { user: req.session.user });
});

router.post('/login', function(req, res, next) {
  var quander = createQuanderClient();

  quander.login(req.body.username, req.body.password).then((accessToken) => {
    req.session.user = accessToken;

    res.redirect('/accounts');
  });
});

router.get('/accounts', function(req, res, next) {
  var quander = initQuanderClientFromToken(req.session.user, req, res);

  if (!quander) {
    res.redirect('/');
    return;
  }

  var accountManager = quander.createResource('accounts');

  accountManager.getList().then(function (accounts) {
    console.log(accounts);
    res.render('accounts.twig', { accounts: accounts.data });
  });
});

router.get('/projects', function(req, res, next) {
  var quander = initQuanderClientFromToken(req.session.user, req, res);

  if (!quander) {
    res.redirect('/');
    return;
  }

  var projectManager = quander.createResource('projects', {account: req.query.account});

  projectManager.getList().then(function (projects) {
    console.log(projects);
    res.render('projects.twig', { projects: projects.data });
  });
});

router.get('/media', function(req, res, next) {
  var quander = initQuanderClientFromToken(req.session.user, req, res);

  if (!quander) {
    res.redirect('/');
    return;
  }

  var mediaManager = quander.createResource('medias', {project: req.query.project});

  mediaManager.getList().then(function (media) {
    console.log(media);
    res.render('media.twig', { media: media.data });
  });
});

module.exports = router;

function initQuanderClientFromToken(user, req, res) {
  if (!req.session.user) {
    return;
  }

  var quander = createQuanderClient();

  try {
    quander.setAccessToken(user);
  } catch (e) {
    console.log(e);
    if (e instanceof QuanderSdkError && QuanderSdkError.TOKEN_EXPIRED === e.errorCode) {
      // Call the api to request the new access token
      return quander.getRefreshedToken(user).then((token) => {
        quander.setAccessToken(token);

        req.session.user = token;
      }).catch((e) => {
        // Refresh token also expired or not valid
        if (e instanceof QuanderApiError && QuanderApiError.REFRESH_TOKEN_INVALID === e.errorCode) {
        }
        res.redirect('/');
      });
    } else {
      res.redirect('/');
    }
  }

  quander.setAccessToken(user);

  return quander;
}

function createQuanderClient() {
  Quander.enableDebug();
  return new Quander({
    baseUrl: `http://${process.env.QUANDER_DOMAIN || 'dev'}.quander.io/api`,
    tokenUrl: `http://${process.env.QUANDER_DOMAIN || 'dev'}.quander.io`,
    clientId: process.env.QUANDER_APP_ID || '2_e53eq7eho7k8c0o8o08occ004w000okwkkw04gog40gwcs80k',
    clientSecret: process.env.QUANDER_APP_SECRET || '1a1t2u7b9540ggkk8s0gc4wcwcwwow40k4osw40cwo44swcoo0'
  });
}
